# XO

status: in-progress

For lack of a better name

A multi-tenancy architecture project boilerplate, incorporating the best tools in their respective industries/domains.

## Languages

- Golang
- GraphQL
- Protobuf3

## Database

- Redis
- PostgreSQL
- CockroachDB

## Analytics

- Grafana
- Prometheus

## Monorepo MGMT

- Bazel

## Infrastructure MGMT

- Terraform