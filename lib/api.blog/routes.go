package main

import (
	"net/http"

	srv "github.com/labstack/echo"
)

func getArticle(c srv.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, id)
}

func createArticle(c srv.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, id)
}

func updateArticle(c srv.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, id)
}

func deleteArticle(c srv.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, id)
}

func getArticles(c srv.Context) error {
	return c.String(http.StatusOK, "x")
}
