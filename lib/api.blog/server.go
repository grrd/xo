package main

import (
	srv "github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"io"
	"github.com/labstack/gommon/log"
)

func configureAndStartServer(conf configurations, serverLogWriter, httpLogWriter io.Writer) {
	s := srv.New()

	// Hide the official Echo banner; less console noise
	s.HideBanner = true

	// Set the web server logging level and output
	s.Logger.SetLevel(log.Lvl(conf.Logger().Level))
	s.Logger.SetOutput(serverLogWriter)

	// Configure middleware
	// -----------------------

	// [MIDDLEWARE] logger
	s.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: conf.Logger().Format,
		Output: httpLogWriter,
	}))

	// [MIDDLEWARE] top-level exception handler
	s.Use(middleware.Recover())

	// Register service routes
	registerRoutes(s)

	s.Logger.Fatal(s.Start(conf.ServerAddr()))
}

func registerRoutes(s *srv.Echo) {
	s.GET("/articles", getArticles)
	s.GET("/articles/:id", getArticle)
	s.POST("/articles", createArticle)
	s.PUT("/articles/:id", updateArticle)
	s.DELETE("/articles/:id", deleteArticle)
}