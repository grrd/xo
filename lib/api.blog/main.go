package main

import (
	"io"
	"os"
)

func main() {
	// load the configurations before doing anything else
	conf, err := loadConfig("./config")
	if err != nil {
		panic(err)
	}

	var serverLogWriter io.Writer = os.Stdout
	var httpLogWriter io.Writer = os.Stdout

	if !conf.IsDevelopment() || (conf.IsDevelopment() && conf.EnvVar("LOCAL_SVC_COMPOSITION") != "isolated") {
		logHandler := newLogHandler()
		serverLogWriter = logHandler.ServerLogWriter()
		httpLogWriter = logHandler.HTTPLogWriter()

		logHandler.Start()
	}

	configureAndStartServer(
		conf,
		serverLogWriter,
		httpLogWriter,
	)
}
