# Blog Service

## Environment variables

### `SVC_LOCAL_COMPOSITION`

determine whether services should communicate with each other
during local development.

values:
- `isolated`
- `integrated:manual`
- `integrated:containers`

### `SVC_ENV`

the current environment/stage for the service.

values:
- `development`
- `staging`
- `production`