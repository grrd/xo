package main

import (
	"os"
	"fmt"
	"regexp"
	"strings"
	"github.com/pkg/errors"
	"github.com/BurntSushi/toml"
	"github.com/labstack/gommon/log"
	"github.com/mitchellh/mapstructure"
	customenv "github.com/joho/godotenv"
	embedder "github.com/gobuffalo/packr"
)

const (
	configVarStrStart = "{{{"
	configVarStrEnd = "}}}"
	configVarRgxStr =  "{{{([A-Za-z0-9_]+)}}}"
)


var (
	confVarRgx *regexp.Regexp

	// use empty string value if default value must be derived from
	// the environment variable itself
	defaultEnvVars = map[string]string{
		"SVC_ENV": "development",
		"SVC_LOCAL_COMPOSITION": "isolated",
	}
)

type (
	configurations interface {
		EnvVar(string) string
		CurrentEnv() string
		IsDevelopment() bool
		Logger() *loggerConfig
		Server() *serverConfig
		ServerAddr() string
	}
)

type (
	config struct {
		embedded embedder.Box
		env string
		logger loggerConfig
		server serverConfig
		envVars map[string]string
	}

	loggerConfig struct {
		Level int8 `toml:"level"`
		Format string `toml:"format"`
	}

	serverConfig struct {
		Host string `toml:"host"`
		Port int32 `toml:"port"`
	}
)

// EnvVar provides lookup for injected env vars
func (c *config) EnvVar(e string) string {
	return c.envVars[e]
}

// Logger returns a reference to the full logger config struct
func (c *config) Logger() *loggerConfig {
	return &c.logger
}

// Server returns a reference to the full server config struct
func (c *config) Server() *serverConfig {
	return &c.server
}

// ServerAddr convenience server port accessor
func (c *config) ServerAddr() string {
	return fmt.Sprintf("%s:%d", c.server.Host, c.server.Port)
}

// IsDevelopment convenience development env flag accessor
func (c *config) IsDevelopment() bool {
	// this env var may not necessarily exist in non-local settings so a negated check is performed here
	return c.env == "development" || (c.env != "staging" && c.env != "production")
}

// CurrentEnv convenience env accessor
func (c *config) CurrentEnv() string {
	return c.env
}

// loadConfig initializes the configurations and returns the config struct
func loadConfig(configDirPath string) (*config, error) {
	log.SetPrefix("init.config")
	log.Debugf("Loading embedded config files from %d", configDirPath)

	confVarRgx, _ = regexp.Compile(configVarRgxStr)

	cfg := config{}
	cfg.embedded = embedder.NewBox(configDirPath)

	// create new empty config instance
	var envVars map[string]string

	// extract and parse custom environment variables
	contentStr, err := cfg.embedded.MustString(".env")
	if err != nil {
		// use defaults if file does not exist (typically the case in staging or production)
		log.Info("Custom env file not found, using system's env vars or falling back to defaults where applicable.")
		for k, v := range defaultEnvVars {
			if v == "" {
				envVars[k] = os.Getenv(k) // use actual environment variable
			} else {
				envVars[k] = v // if non-empty string, use the hardcoded default value
			}
		}
	} else {
		// use the custom env file and throw a warning (it's assumed that this is dev mode)
		log.Info("Custom env file found. This should not be present in UAT or Prod.")
		envVars, err = customenv.Unmarshal(contentStr)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to parse embedded custom environment variables file")
		}
	}
	cfg.envVars = envVars
	cfg.env = envVars["SVC_ENV"]

	// extract and parse logger configuration
	var loggerConf loggerConfig
	if err = cfg.loadAndParse("logger", &loggerConf); err != nil {
		return nil, err
	}
	cfg.logger = loggerConf

	// extract and parse server configuration
	var serverConf serverConfig
	if err = cfg.loadAndParse("server", &serverConf); err != nil {
		return nil, err
	}
	cfg.server = serverConf

	// END
	return &cfg, nil
}

// private helper method
func (c *config) loadAndParse(confName string, structure interface{}) error {
	var rawStructure map[string]map[string]interface{}

	// load config file from the embedded directory.
	// all config files are assumed to have the following format: <confName>.conf.toml
	contentStr, err := c.embedded.MustString(fmt.Sprintf("%s.conf.toml", confName))
	if err != nil {
		return errors.Wrapf(err, "Failed to load embedded config file for '%s'", confName)
	}

	// decode the config string into an intermediate map
	// so we can perform string interpolation for custom variables.
	if _, err = toml.Decode(contentStr, &rawStructure); err != nil {
		return errors.Wrapf(err, "Failed to decode config string for '%s' into intermediate map", confName)
	}

	// each config file has a TOML table for each environment,
	// plus one special table called 'variables' (TOML does not include native support for variables)
	// extract the appropriate table according to the current environment/stage.
	configMap := rawStructure[c.env]

	// find and replace variables in the env table where applicable.
	for k, v := range configMap {
		switch val := v.(type) {
		case string:
			if strings.HasPrefix(val,configVarStrStart) && strings.HasSuffix(val, configVarStrEnd) {
				// the value is a variable that needs to be interpolated.
				// capture any alphanumerics encapsulated in the triple curly braces
				// and use it to lookup the 'variables' table.
				varName := confVarRgx.FindStringSubmatch(val)[1]
				configMap[k] = rawStructure["variables"][varName]
			}
		}
	}

	// finally, take the interpolated map and decode it into the specified struct.
	if err = mapstructure.Decode(configMap, structure); err != nil {
		return errors.Wrapf(err, "Failed to decode config map for '%s' into given structure", confName)
	}

	return nil
}
