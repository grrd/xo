package main

import (
	"fmt"
)

type (
	LogHandler interface {
		StartLogCollector()
		ServerLogWriter() *serverLogWriter
		HttpLogWriter() *httpLogWriter
	}
)

type (
	logHandler struct {
		serverLogCh chan []byte
		httpLogCh chan []byte

		serverLogWriter *serverLogWriter
		httpLogWriter *httpLogWriter
	}

	// httpLogWriter implements io.Writer
	httpLogWriter struct {
		logCh *chan []byte
	}

	// serverLogWriter implements io.Writer
	serverLogWriter struct {
		logCh *chan []byte
	}
)

// newLogHandler returns a new logHandler instance
func newLogHandler() *logHandler {
	slCh := make(chan []byte, 1024)
	hlCh := make(chan []byte, 1024)

	return &logHandler{
		serverLogCh:slCh,
		httpLogCh:hlCh,
		serverLogWriter: &serverLogWriter{&slCh},
		httpLogWriter:&httpLogWriter{&hlCh},
	}
}

// Start launches a goroutine func that waits for and parses incoming logs
func (lh *logHandler) Start() {
	go func() {
		for {
			select {
			case serverLog := <-lh.serverLogCh:
				go handleServerLog(serverLog)
			case httpLog := <-lh.httpLogCh:
				go handleHTTPLog(httpLog)
			}
		}
	}()
}

// ServerLogWriter returns the serverLogWriter instance
func (lh *logHandler) ServerLogWriter() *serverLogWriter {
	return lh.serverLogWriter
}

// HTTPLogWriter returns the httpLogWriter instance
func (lh *logHandler) HTTPLogWriter() *httpLogWriter {
	return lh.httpLogWriter
}

// Write method for serverLogWriter to implement io.Writer
func (slw *serverLogWriter) Write(log []byte) (int, error) {
	b := make([]byte, len(log))
	copy(b, log)
	*slw.logCh <- b
	return len(log), nil
}

// Write method for httpLogWriter to implement io.Writer
func (hlw *httpLogWriter) Write(log []byte) (int, error) {
	b := make([]byte, len(log))
	copy(b, log)
	*hlw.logCh <- b
	return len(log), nil
}

func handleServerLog(sLog []byte) {
	fmt.Println(string(sLog))
}

func handleHTTPLog(hLog []byte) {
	fmt.Println(string(hLog))
}


