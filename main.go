package main

import (
	"fmt"
	"time"
	"math/rand"
)

func main() {
	hub := make(chan string, 120)

	go minion(hub, "a")
	go minion(hub,"b")
	go minion(hub, "c")
	go minion(hub, "d")

	terminal(hub)
}

func minion(ch chan <- string, msg string) {
	for i := 0; ; i++ {
		ch <- fmt.Sprintf("[%d] %s", i, msg)
		time.Sleep(time.Duration(rand.Intn(1e2)) * time.Millisecond)
	}
}

func terminal(ch <- chan string) {
	for {
		time.Sleep(time.Duration(rand.Intn(1e2)) * time.Millisecond)
		fmt.Printf("[%d] RECEIVED: %q\n", len(ch), <-ch)
	}
}