# Libraries

## Service

Private cloud services

## API

Public-facing/accessible services

## App

A client app typically served with a web server or cloud storage service

## List

- `svc.citadel`
- `svc.hub`
- `app.blog`
- `app.forum`
- `app.website`
- `app.admin`
- `app.secrets`
- `app.ops`